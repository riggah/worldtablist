package me.riggah.worldtablist;

import java.util.logging.Logger;

import net.minecraft.server.v1_7_R1.Packet;
import net.minecraft.server.v1_7_R1.PacketPlayOutPlayerInfo;

import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_7_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class WorldTablist extends JavaPlugin
{
	public static final Logger log = Bukkit.getLogger();
	
	@Override
	public void onEnable()
	{
		log.info("WorldTablist has been enabled!");
		log.info("WorldTablist v" + this.getDescription().getVersion() + " by riggah");
		new WTListener(this);
	}
	
	@Override
	public void onDisable()
	{
		log.info("WorldTablist has been disabled!");
	}
	
	public void check(Player player)
	{
		for(Player plr : Bukkit.getOnlinePlayers())
		{
			if(!plr.getWorld().equals(player.getWorld()))
					{
				      Packet packet1 = new PacketPlayOutPlayerInfo(plr.getName(), false, 0);
				      ((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet1);
				      Packet packet2 = new PacketPlayOutPlayerInfo(player.getName(), false, 0);
				      ((CraftPlayer) plr).getHandle().playerConnection.sendPacket(packet2);
					}
		}
	}

}
