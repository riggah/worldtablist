package me.riggah.worldtablist;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerJoinEvent;

public class WTListener implements Listener
{
	private WorldTablist plugin;

	public WTListener(WorldTablist instance) 
	{
		this.plugin = instance;
		this.plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}
	
	@EventHandler
	public void onWorldChange(PlayerChangedWorldEvent ev)
	{
		Player p = ev.getPlayer();
		plugin.check(p);
		for(Player plr : Bukkit.getOnlinePlayers())
		{
			
				plugin.check(plr);
			
			
		}
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent ev)
	{
		Player p = ev.getPlayer();
		plugin.check(p);
		for(Player plr : Bukkit.getOnlinePlayers())
		{
			
				plugin.check(plr);
			
			
		}
	}
	

}
